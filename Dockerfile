FROM nginx:1.18
ARG BRANCH
COPY . /usr/share/nginx/html
RUN sed "s/%BRANCH%/$BRANCH/g" -i /usr/share/nginx/html/index.html

